﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            string LFI = "2020-02-07 08:00:00.000"; //Comentario desde gitlab a VS
            string LFF = "2020-02-07 09:54:24.000";
            string PFI = "2020-02-07 07:00:00.000";
            string PFF = "2020-02-07 08:00:59.000";
            DateTime LFI1 = DateTime.Parse(LFI);
            DateTime LFF1 = DateTime.Parse(LFF);
            DateTime PFI1 = DateTime.Parse(PFI); //comentario
            DateTime PFF1 = DateTime.Parse(PFF);
            if (LFI1.AddSeconds(-LFI1.Second) >= PFI1.AddSeconds(-PFI1.Second) && LFF1.AddSeconds(-LFF1.Second) <= PFF1.AddSeconds(-PFF1.Second))
            {
                Console.WriteLine("Fecha Final y Inicial estan dentro del permiso 00");
                Console.WriteLine(LFI1.AddSeconds(-LFI1.Second));
            }
            if (LFI1 >= PFI1 && LFF1 <= PFF1)
            {
                Console.WriteLine("Fecha Final y Inicial estan dentro del permiso");
                Console.WriteLine(LFI1.TimeOfDay);
            }
            if (LFI1 >= PFI1 && LFF1 > PFF1)
            {
                Console.WriteLine("Permiso cubre fecha inicio pero no fecha final ");
            }
            if (LFI1 < PFI1 && LFF1 <= PFF1)
            {
                Console.WriteLine("Permiso cubre fecha final pero no fecha inicio");
            }
            int result = DateTime.Compare(LFI1, PFI1);
            int result2 = DateTime.Compare(LFF1, PFF1);
            if (result >= 0 && result2 <= 0) { Console.WriteLine("Fechas Iguales"); }
            if (result >= 0 && result2 > 0) { Console.WriteLine(LFI1 +" > " + PFI1); }
            if (result < 0 && result2 <= 0) { Console.WriteLine(LFI1 +" < " + PFI1); }
            Console.ReadLine();
        }
    }
}
